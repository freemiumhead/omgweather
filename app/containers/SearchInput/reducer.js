import { GET_SUGGESTIONS_START, GET_SUGGESTIONS_SUCCESS, GET_SUGGESTIONS_FAILED } from './constants';

const initialState = {
  isFetching: false,
  isFailed: false,
  isSuccess: false,
};

const searchInputReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SUGGESTIONS_START:
      return { ...state, isFetching: true };
    case GET_SUGGESTIONS_SUCCESS:
      return { ...state, isFetching: false, isSuccess: true };
    case GET_SUGGESTIONS_FAILED:
      return { ...state, isFetching: false, isFailed: true };
    default:
      return state;
  }
};

export default searchInputReducer;
