import { getRequest } from '../../utils/api';
import { weatherApi } from '../../utils/apiConstants';

const GET_WEATHER_API_PATH = `${weatherApi}/weather`;

export const getWeatherData = (city, country) =>
  getRequest(GET_WEATHER_API_PATH, {
    q: `${city},${country}`,
    mode: 'json',
    units: 'metric',
    APPID: '8a8f9df57820427937b6ae7364af721e',
  });
