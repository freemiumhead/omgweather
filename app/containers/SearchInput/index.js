import { connect } from 'react-redux';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';

import saga from './saga';
import reducer from './reducer';

import { getSuggestionsStart } from './actions';

import SearchInputComponent from '../../components/SearchInputComponent';

const actions = {
  getSuggestionsStart,
};

export default SearchInputComponent
  |> connect(
    null,
    actions,
  )
  |> injectSaga({ key: 'searchInput', saga })
  |> injectReducer({ key: 'searchInput', reducer });
