/**
 *
 * Asynchronously loads the component for SearchInput
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
