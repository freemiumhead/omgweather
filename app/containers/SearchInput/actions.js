/*
 *
 * SearchInput actions
 *
 */

import { GET_SUGGESTIONS_START, GET_SUGGESTIONS_SUCCESS, GET_SUGGESTIONS_FAILED } from './constants';

export const getSuggestionsStart = payload => ({
  type: GET_SUGGESTIONS_START,
  payload,
});
export const getSuggestionsSuccess = payload => ({
  type: GET_SUGGESTIONS_SUCCESS,
  payload,
});
export const getSuggestionsFailed = () => ({ type: GET_SUGGESTIONS_FAILED });
