import { takeLatest, put, select } from 'redux-saga/effects';
import { map, head, last, prop, prepend, filter, isEmpty } from 'ramda';
import { geocodeByAddress } from 'react-places-autocomplete';

import { getWeatherData } from './api';
import { getSuggestionsFailed } from './actions';
import { getFullWeatherStart, getNewCityInfoStart } from '../WeatherBlock/actions';
import { addInLatest } from '../Latest/actions';
import { GET_SUGGESTIONS_START, LATEST_ITEMS_LOCALSTORAGE } from './constants';
import { getLatestItemsDataState } from '../Latest/selectors';

/**
 * here we get place info from google suggestions
 * if we already have city in state, we just set that city to active
 * if that new city, we add it to latestSearchItems array and save in localstorage
 *
 * @param {{payload: string}} { payload } address from google autocomplite
 */
function* searchInputSaga({ payload }) {
  try {
    /** show loader */
    yield put(getNewCityInfoStart());

    /**
     *  @type {Array} get place info from places API
     */
    const data = yield geocodeByAddress(payload);

    const placeInfo = data |> head;

    const cityInfo = placeInfo |> prop('address_components');
    const city = cityInfo |> head |> prop('long_name');
    const country = cityInfo |> last |> prop('short_name');

    /**
     * get current weather in current place
     */
    const weatherData = yield getWeatherData(city, country);

    const latestItems = yield select(getLatestItemsDataState);
    const itemExist = latestItems |> filter(({ id }) => id === weatherData.id);

    yield put(getFullWeatherStart(weatherData.id));

    if (isEmpty(itemExist)) {
      const updatedLatestItems = latestItems |> prepend(weatherData);
      const citiesId = updatedLatestItems |> map(({ id }) => id);
      yield put(addInLatest(updatedLatestItems));
      localStorage.setItem(LATEST_ITEMS_LOCALSTORAGE, JSON.stringify(citiesId));
    }
  } catch (error) {
    yield put(getSuggestionsFailed());
  }
}

export default function*() {
  yield takeLatest(GET_SUGGESTIONS_START, searchInputSaga);
}
