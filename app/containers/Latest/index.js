import { connect } from 'react-redux';
import { lifecycle } from 'recompose';

import injectReducer from '../../utils/injectReducer';
import reducer from './reducer';

import injectSaga from '../../utils/injectSaga';
import saga from './saga';

import { addInLatest, getCurrentWeatherStart } from './actions';
import { getFullWeatherStart } from '../WeatherBlock/actions';

import { getLatestItemsIsFetchingState, getLatestItemsDataState } from './selectors';
import { getActiveCityIdState } from '../WeatherBlock/selectors';
import { LATEST_ITEMS_LOCALSTORAGE } from '../SearchInput/constants';

import LatestItems from '../../components/LatestItems';

const componentLifecycle = {
  componentDidMount() {
    const initLatestItemsDataState = localStorage.getItem(LATEST_ITEMS_LOCALSTORAGE);
    if (initLatestItemsDataState !== null) {
      this.props.getCurrentWeatherStart(JSON.parse(initLatestItemsDataState));
    }
  },
};

const mapStateToProps = state => ({
  latestCards: getLatestItemsDataState(state),
  activeCityId: getActiveCityIdState(state),
  isFetching: getLatestItemsIsFetchingState(state),
});

const actions = {
  addInLatest,
  getCurrentWeatherStart,
  getFullWeatherStart,
};

export default LatestItems
  |> lifecycle(componentLifecycle)
  |> connect(
    mapStateToProps,
    actions,
  )
  |> injectSaga({ key: 'latestSaga', saga })
  |> injectReducer({ key: 'latest', reducer });
