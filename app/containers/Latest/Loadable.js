/**
 *
 * Asynchronously loads the component for Latest
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
