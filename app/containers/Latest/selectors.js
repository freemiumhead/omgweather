const getLatestItemsState = state => state?.latest;

export const getLatestItemsIsFetchingState = state => getLatestItemsState(state)?.isFetching;
export const getLatestItemsDataState = state => getLatestItemsState(state)?.items;
