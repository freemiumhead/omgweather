import {
  ADD_IN_LATEST,
  GET_CURRENT_WEATHER_START,
  GET_CURRENT_WEATHER_SUCCESS,
  GET_CURRENT_WEATHER_FAILED,
} from './constants';

const initialState = {
  isFetching: false,
  items: [],
};

const latestReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_IN_LATEST:
      return { ...state, items: action.payload };

    case GET_CURRENT_WEATHER_START:
      return { ...state, isFetching: true };
    case GET_CURRENT_WEATHER_SUCCESS:
      return { ...state, isFetching: false, items: action.payload };
    case GET_CURRENT_WEATHER_FAILED:
      return { ...state, isFetching: false };
    default:
      return state;
  }
};

export default latestReducer;
