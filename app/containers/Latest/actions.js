/*
 *
 * Latest actions
 *
 */

import {
  ADD_IN_LATEST,
  GET_CURRENT_WEATHER_START,
  GET_CURRENT_WEATHER_SUCCESS,
  GET_CURRENT_WEATHER_FAILED,
} from './constants';

export const addInLatest = payload => ({ type: ADD_IN_LATEST, payload });

export const getCurrentWeatherStart = payload => ({ type: GET_CURRENT_WEATHER_START, payload });
export const getCurrentWeatherSuccess = payload => ({ type: GET_CURRENT_WEATHER_SUCCESS, payload });
export const getCurrentWeatherFailed = () => ({ type: GET_CURRENT_WEATHER_FAILED });
