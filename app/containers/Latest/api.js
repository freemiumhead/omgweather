import { join } from 'ramda';

import { getRequest } from '../../utils/api';
import { weatherApi } from '../../utils/apiConstants';

const GET_WEATHER_FOR_LIST_OF_CITIES_PATH = `${weatherApi}/group`;

export const getCurrentWeather = citiesId => {
  const ids = citiesId |> join(',');
  return getRequest(GET_WEATHER_FOR_LIST_OF_CITIES_PATH, {
    APPID: '8a8f9df57820427937b6ae7364af721e',
    id: ids,
    units: 'metric',
  });
};
