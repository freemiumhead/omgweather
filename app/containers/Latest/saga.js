import { takeLatest, put } from 'redux-saga/effects';

import { getCurrentWeather } from './api';
import { getCurrentWeatherSuccess, getCurrentWeatherFailed } from './actions';
import { GET_CURRENT_WEATHER_START } from './constants';

function* latestSaga({ payload }) {
  try {
    const { list } = yield getCurrentWeather(payload);

    yield put(getCurrentWeatherSuccess(list));
  } catch (error) {
    yield put(getCurrentWeatherFailed());
  }
}

export default function*() {
  yield takeLatest(GET_CURRENT_WEATHER_START, latestSaga);
}
