import { takeLatest, put } from 'redux-saga/effects';

import { getFullWeather } from './api';
import { getFullWeatherSuccess, getFullWeatherFailed } from './actions';
import { GET_FULL_WEATHER_START } from './constants';
import history from '../../utils/history';

function* weatherBlockSaga({ payload }) {
  try {
    const data = yield getFullWeather(payload);
    yield put(getFullWeatherSuccess(data));
    history.push({ pathname: payload });
  } catch (error) {
    yield getFullWeatherFailed();
  }
}

export default function*() {
  yield takeLatest(GET_FULL_WEATHER_START, weatherBlockSaga);
}
