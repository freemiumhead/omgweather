const getWeatherBlockState = state => state.weatherBlock;

export const getWeatherBlockFetchingState = state => getWeatherBlockState(state)?.isFetching;
export const getWeatherBlockDataState = state => getWeatherBlockState(state)?.data;
export const getActiveCityIdState = state => getWeatherBlockState(state)?.activeCityId;
