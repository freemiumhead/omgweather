import { getRequest } from '../../utils/api';
import { weatherApi } from '../../utils/apiConstants';

const GET_FULL_WEATHER_API_PATH = `${weatherApi}/forecast`;

export const getFullWeather = id =>
  getRequest(GET_FULL_WEATHER_API_PATH, {
    id,
    APPID: '8a8f9df57820427937b6ae7364af721e',
    units: 'metric',
  });
