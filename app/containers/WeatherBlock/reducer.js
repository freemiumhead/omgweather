import {
  GET_FULL_WEATHER_START,
  GET_FULL_WEATHER_SUCCESS,
  GET_FULL_WEATHER_FAILED,
  GET_NEW_CITY_INFO_START,
  GET_NEW_CITY_INFO_SUCCESS,
} from './constants';

const initialState = {
  isFetching: false,
  data: [],
  activeCityId: 0,
};

const weatherCardContainerReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FULL_WEATHER_START:
      return { ...state, isFetching: true, activeCityId: action.payload };
    case GET_FULL_WEATHER_SUCCESS:
      return { ...state, isFetching: false, data: action.payload };
    case GET_FULL_WEATHER_FAILED:
      return { ...state, isFetching: false };

    case GET_NEW_CITY_INFO_START:
      return { ...state, isFetching: true };
    case GET_NEW_CITY_INFO_SUCCESS:
      return { ...state, isFetching: false };
    default:
      return state;
  }
};

export default weatherCardContainerReducer;
