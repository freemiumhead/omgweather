/*
 *
 * WeatherCardContainer actions
 *
 */

import {
  GET_FULL_WEATHER_START,
  GET_FULL_WEATHER_SUCCESS,
  GET_FULL_WEATHER_FAILED,
  GET_NEW_CITY_INFO_START,
  GET_NEW_CITY_INFO_SUCCESS,
} from './constants';

export const getFullWeatherStart = payload => ({ type: GET_FULL_WEATHER_START, payload });
export const getFullWeatherSuccess = payload => ({ type: GET_FULL_WEATHER_SUCCESS, payload });
export const getFullWeatherFailed = () => ({ type: GET_FULL_WEATHER_FAILED });

export const getNewCityInfoStart = () => ({ type: GET_NEW_CITY_INFO_START });
export const getNewCityInfoSuccess = () => ({ type: GET_NEW_CITY_INFO_SUCCESS });
