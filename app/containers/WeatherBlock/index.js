import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { lifecycle } from 'recompose';

import injectSaga from '../../utils/injectSaga';
import injectReducer from '../../utils/injectReducer';

import saga from './saga';
import reducer from './reducer';

import { getFullWeatherStart } from './actions';
import { getWeatherBlockFetchingState, getWeatherBlockDataState, getActiveCityIdState } from './selectors';
import { getLatestItemsDataState } from '../Latest/selectors';

import WeatherBlockComponent from '../../components/WeatherBlockComponent';

const componentLifecycle = {
  componentDidMount() {
    if (this.props.match.params.place) {
      this.props.getFullWeatherStart(this.props.match.params.place);
    }
  },
};

const mapStateToProps = state => ({
  isFetching: getWeatherBlockFetchingState(state),
  weather: getWeatherBlockDataState(state),
  latestItems: getLatestItemsDataState(state),
  aciveCity: getActiveCityIdState(state),
});

const actions = {
  getFullWeatherStart,
};

export default WeatherBlockComponent
  |> lifecycle(componentLifecycle)
  |> connect(
    mapStateToProps,
    actions,
  )
  |> injectSaga({ key: 'weatherBlock', saga })
  |> injectReducer({ key: 'weatherBlock', reducer })
  |> withRouter;
