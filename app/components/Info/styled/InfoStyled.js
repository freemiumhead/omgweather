import styled from 'styled-components';

import { transition } from '../../../utils/styled';

const InfoStyled = styled.a.attrs({
  target: '_blank',
})`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  background-color: #fafafa;
  text-decoration: none;
  text-transform: uppercase;
  filter: grayscale(1);
  color: #fff;
  ${transition};
  overflow: hidden;

  &:hover {
    filter: grayscale(0);
    ${transition};

    svg {
      animation-name: animeHover;
      animation-duration: 1s;
      animation-iteration-count: infinite;
    }

    span {
      color: #fafafa;
      ${transition};
    }
  }

  svg {
    height: 80%;
    animation-name: anime;
    animation-duration: 1s;
    animation-iteration-count: infinite;
  }

  span {
    position: absolute;
    top: 47%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: -1;
    font-size: 188px;
    font-weight: 700;
    line-height: 1;
    ${transition};
  }

  @keyframes anime {
    0% {
      transform: scale(1);
    }
    50% {
      transform: scale(1.01);
    }
    100% {
      transform: scale(1);
    }
  }

  @keyframes animeHover {
    0% {
      transform: scale(1);
    }
    50% {
      transform: scale(1.05);
    }
    100% {
      transform: scale(1);
    }
  }
`;

export default InfoStyled;
