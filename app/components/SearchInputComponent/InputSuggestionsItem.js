import React from 'react';
import { withHandlers } from 'recompose';

import InputSuggestionsItemStyled from './styled/InputSuggestionsItemStyled';

const componentHandlers = () => ({
  handle: ({ handleChange, handleSelect, suggestion: { description } }) => () => {
    handleChange(description);
    handleSelect(description);
  },
});

const InputSuggestionsItem = ({ suggestion, active, handle, getSuggestionItemProps }) => (
  <InputSuggestionsItemStyled {...getSuggestionItemProps(suggestion, { onClick: handle, active })}>
    <span>{suggestion.description}</span>
  </InputSuggestionsItemStyled>
);

export default InputSuggestionsItem |> withHandlers(componentHandlers);
