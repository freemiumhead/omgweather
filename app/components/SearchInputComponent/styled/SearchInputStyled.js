import styled, { css } from 'styled-components';

const SearchInputStyled = styled.input.attrs({
  type: 'text',
  placeholder: 'search',
})`
  ${({ theme }) => {
    const { colors, transition, shadows } = theme;

    return css`
      width: 70%;
      height: 60px;
      border: 1px solid ${colors.primary};
      box-shadow: ${shadows.default};
      text-align: center;
      overflow: hidden;
      text-transform: uppercase;
      font-weight: 700;
      font-size: 22px;
      padding: 0 20px;
      ${transition.default};

      &::placeholder {
        color: ${colors.primary};
        text-transform: uppercase;
        font-weight: 700;
      }

      &:focus,
      &:hover {
        outline: none;
        box-shadow: ${shadows.defaultHover};
        transform: ${transition.shadowTransform};
        ${transition.default};
      }

      &:focus {
        &::placeholder {
          opacity: 0;
        }
      }
    `;
  }};
`;

export default SearchInputStyled;
