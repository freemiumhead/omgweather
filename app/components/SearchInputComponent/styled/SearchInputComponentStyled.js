import styled, { css } from 'styled-components';

const SearchInputComponentStyled = styled.div`
  ${({ theme, loading }) => css`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;

    &:after {
      content: 'Loading...';
      position: absolute;
      left: 50%;
      top: 100%;
      transform: translateX(calc(-50% + 10px));
      width: 70%;
      height: 40px;
      display: ${loading ? 'flex' : 'none'};
      justify-content: center;
      align-items: center;
      font-size: 14px;
      text-transform: uppercase;
      background-color: #fff;
      border: 1px solid ${theme.colors.primary};
      font-weight: 700;
      color: ${theme.colors.primary};
    }
  `};
`;

export default SearchInputComponentStyled;
