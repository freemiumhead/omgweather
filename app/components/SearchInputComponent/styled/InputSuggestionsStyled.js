import styled, { css } from 'styled-components';

const InputSuggestionsStyled = styled.ul`
  ${({ theme }) => css`
    position: absolute;
    top: 100%;
    left: 50%;
    transform: translateX(calc(-50% + 10px));
    width: 70%;
    margin: 0;
    list-style: none;
    padding: 0;
    border: 4px solid ${theme.colors.primary};
    box-shadow: 4px 4px 5px ${theme.colors.primary};
    background-color: #fff;
  `};
`;

export default InputSuggestionsStyled;
