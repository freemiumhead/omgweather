import styled, { css } from 'styled-components';

const InputSuggestionsItemStyled = styled.li`
  ${({ theme, active }) => css`
    text-align: center;
    padding: 10px;
    font-size: 20px;
    font-weight: 700;
    ${theme.transition.default};

    &:not(:last-child) {
      border-bottom: 4px solid ${theme.colors.primaryLight};
    }

    ${active &&
      css`
        background-color: ${theme.colors.primaryLight};
        cursor: pointer;
        ${theme.transition.default};
      `} &:hover {
      background-color: ${theme.colors.primaryLight};
      cursor: pointer;
      ${theme.transition.default};
    }
  `};
`;

export default InputSuggestionsItemStyled;
