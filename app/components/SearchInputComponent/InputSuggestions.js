import React from 'react';
import { func, array } from 'prop-types';
import { isEmpty } from 'ramda';
import { branch, renderNothing } from 'recompose';

import InputSuggestionsItem from './InputSuggestionsItem';

import InputSuggestionsStyled from './styled/InputSuggestionsStyled';

const InputSuggestions = ({ suggestions, handleChange, getSuggestionItemProps, handleSelect }) => (
  <InputSuggestionsStyled>
    {suggestions.map(suggestion => (
      <InputSuggestionsItem
        active={suggestion.active}
        key={suggestion.id}
        {...{
          suggestion,
          handleChange,
          getSuggestionItemProps,
          handleSelect,
        }}
      />
    ))}
  </InputSuggestionsStyled>
);

InputSuggestions.propTypes = {
  suggestions: array,
  handleChange: func,
  handleSelect: func,
  getSuggestionItemProps: func,
};

const isHidden = ({ suggestions }) => suggestions |> isEmpty;

export default InputSuggestions |> branch(isHidden, renderNothing);
