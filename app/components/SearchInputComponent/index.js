import React from 'react';
import { withStateHandlers, withHandlers } from 'recompose';
import PlacesAutocomplete from 'react-places-autocomplete';

import InputSuggestions from './InputSuggestions';

import SearchInputComponentStyled from './styled/SearchInputComponentStyled';
import SearchInputStyled from './styled/SearchInputStyled';

const state = () => ({
  address: '',
});

const stateUpdaters = {
  handleChange: () => value => ({ address: value }),
};

const componentHandlers = {
  handleSelect: ({ getSuggestionsStart }) => value => {
    getSuggestionsStart(value);
  },
};

const SearchInputComponent = ({ address, handleChange, handleSelect }) => (
  <PlacesAutocomplete value={address} onChange={handleChange} onSelect={handleSelect} highlightFirstSuggestion>
    {({ getInputProps, getSuggestionItemProps, suggestions, loading }) => (
      <SearchInputComponentStyled {...{ loading }}>
        <SearchInputStyled {...getInputProps()} />
        <InputSuggestions
          {...{
            suggestions,
            getSuggestionItemProps,
            handleChange,
            handleSelect,
          }}
        />
      </SearchInputComponentStyled>
    )}
  </PlacesAutocomplete>
);

export default SearchInputComponent |> withHandlers(componentHandlers) |> withStateHandlers(state, stateUpdaters);
