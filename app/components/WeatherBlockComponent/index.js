import React from 'react';
import { array, string } from 'prop-types';
import { map, isEmpty } from 'ramda';
import { branch, renderComponent, withProps } from 'recompose';

import { Line } from 'react-chartjs-2';

import WeatherBlockComponentStyled from './styled/WeatherBlockComponentStyled';
import PlaceholderStyled from './styled/PlaceholderStyled';

const componentProps = ({ weather }) => {
  const labels = weather.list |> map(({ dt_txt: date }) => date);
  const tempAverage = weather.list |> map(({ main: { temp } }) => Math.round(temp));
  const tempMax = weather.list |> map(({ main: { temp_max: maxTemp } }) => Math.round(maxTemp));
  const tempMin = weather.list |> map(({ main: { temp_min: minTemp } }) => Math.round(minTemp));

  return {
    labels,
    datasets: [
      {
        label: 'Temp. average',
        backgroundColor: 'rgba(204, 204, 204, .4)',
        borderColor: 'rgb(204, 204, 204)',
        pointBorderColor: '#b3d4fc',
        data: tempAverage,
      },
      {
        label: 'Temp. max',
        backgroundColor: 'transparent',
        borderColor: '#fcb3b3c9',
        pointBorderColor: '#fcb3b3c9',
        data: tempMax,
      },
      {
        label: 'Temp. min',
        backgroundColor: 'transparent',
        borderColor: '#b3d4fc',
        pointBorderColor: '#b3d4fc',
        data: tempMin,
      },
    ],
    cityName: weather.city.name,
  };
};

const WeatherBlockComponent = ({ labels, datasets, cityName }) => (
  <WeatherBlockComponentStyled>
    <h1>{cityName}</h1>
    <Line data={{ labels, datasets }} width={300} height={100} />
  </WeatherBlockComponentStyled>
);

WeatherBlockComponent.propTypes = {
  labels: array,
  datasets: array,
  cityName: string,
};

const isLoading = ({ isFetching, weather }) => isFetching || isEmpty(weather);
const citiesIsEmpty = ({ latestItems }) => isEmpty(latestItems);
const activeCityNotExist = ({ latestItems, aciveCity }) => !isEmpty(latestItems) && !aciveCity;

export default WeatherBlockComponent
  |> withProps(componentProps)
  |> branch(isLoading, renderComponent(() => <PlaceholderStyled>Loading...</PlaceholderStyled>))
  |> branch(citiesIsEmpty, renderComponent(() => <PlaceholderStyled>Start searching</PlaceholderStyled>))
  |> branch(
    activeCityNotExist,
    renderComponent(() => <PlaceholderStyled>Start searching or choose city</PlaceholderStyled>),
  );
