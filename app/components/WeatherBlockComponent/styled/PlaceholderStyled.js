import styled from 'styled-components';

const PlaceholderStyled = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
  font-size: 30px;
`;

export default PlaceholderStyled;
