import styled from 'styled-components';

const AndStyled = styled.span`
  text-decoration: none;
  font-weight: 400;

  &:last-child {
    display: none;
  }
`;

export default AndStyled;
