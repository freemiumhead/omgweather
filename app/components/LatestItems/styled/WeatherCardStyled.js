import styled, { css } from 'styled-components';

const WeatherCardStyled = styled.div`
  ${({ theme, active }) => {
    const { colors, shadows, transition } = theme;

    return css`
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;
      padding: 20px;
      font-weight: 700;
      border: 1px solid ${colors.primary};
      box-shadow: ${shadows.default};
      margin-bottom: 20px;
      padding-right: 20px;
      ${transition.default};

      &:hover {
        cursor: pointer;
        box-shadow: ${shadows.defaultHover};
        transform: ${transition.shadowTransform};
        ${transition.default};
      }

      ${active &&
        css`
          background-color: ${colors.primaryLight};
          transform: ${transition.shadowActiveTransform};
          box-shadow: ${shadows.defaultActive};
          ${transition.default};
        `};
    `;
  }};
`;

export default WeatherCardStyled;
