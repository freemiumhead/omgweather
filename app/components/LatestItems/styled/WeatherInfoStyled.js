import styled from 'styled-components';
import { media } from '../../../utils/styled/media';

const WeatherInfoStyled = styled.div`
  display: none;

  ${media.lg} {
    width: 100%;
    display: flex;
    justify-content: space-between;
    border-top: 1px solid #ccc;
    margin-top: 10px;
    padding-top: 10px;
  }
`;

export default WeatherInfoStyled;
