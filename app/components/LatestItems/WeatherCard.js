import React from 'react';
import { string, number, array, func } from 'prop-types';
import { withProps, withHandlers } from 'recompose';
import { map } from 'ramda';

import WeatherCardStyled from './styled/WeatherCardStyled';
import WeatherInfoStyled from './styled/WeatherInfoStyled';
import AndStyled from './styled/AndStyled';

const componentProps = ({ main: { temp, temp_min: tempMin, temp_max: tempMax }, activeCityId, id }) => ({
  maximum: Math.round(tempMax),
  minimum: Math.round(tempMin),
  temperature: Math.round(temp),
  active: Number(activeCityId) === Number(id),
});

const componentHandlers = {
  cardHandler: ({ activeCityId, id, getFullWeatherStart }) => () => {
    if (activeCityId !== id) return getFullWeatherStart(id);

    return undefined;
  },
};

const WeatherCard = ({ name, maximum, minimum, temperature, weather, cardHandler, active }) => (
  <WeatherCardStyled onClick={cardHandler} {...{ active }}>
    {name}
    <span>{temperature} C</span>
    <WeatherInfoStyled>
      <div>
        <div>min: {minimum} C</div>
        <div>max: {maximum} C</div>
      </div>
      <div>
        {weather
          |> map(({ description, id }) => (
            <React.Fragment key={id}>
              {description} <AndStyled>and </AndStyled>
            </React.Fragment>
          ))}
      </div>
    </WeatherInfoStyled>
  </WeatherCardStyled>
);

WeatherCard.propTypes = {
  name: string,
  maximum: number,
  minimum: number,
  temperature: number,
  weather: array,
  cardHandler: func,
};

export default WeatherCard |> withHandlers(componentHandlers) |> withProps(componentProps);
