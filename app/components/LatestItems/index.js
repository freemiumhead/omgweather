import React from 'react';
import { map } from 'ramda';
import { branch, renderComponent } from 'recompose';
import { array } from 'prop-types';

import WeatherCard from './WeatherCard';

const LatestItems = ({ latestCards, getFullWeatherStart, activeCityId }) => (
  <>
    {latestCards
      |> map(({ id, ...card }) => <WeatherCard key={id} {...card} {...{ getFullWeatherStart, id, activeCityId }} />)}
  </>
);

LatestItems.propTypes = {
  latestItems: array,
};

const isLoading = ({ isFetching }) => isFetching;
const Loading = () => <div>Loading...</div>;

export default LatestItems |> branch(isLoading, renderComponent(Loading));
