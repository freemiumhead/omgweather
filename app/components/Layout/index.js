import React from 'react';
// import {} from 'recompose';
// import PropTypes from 'prop-types';

import LayoutStyled from './styled/LayoutStyled';
import LayoutItemStyled from './styled/LayoutItemStyled';

import Info from '../Info';
import SearchInput from '../../containers/SearchInput';
import LatestItems from '../../containers/Latest';
import WeatherBlock from '../../containers/WeatherBlock';

const Layout = () => (
  <LayoutStyled>
    <LayoutItemStyled gridArea="info">
      <Info />
    </LayoutItemStyled>
    <LayoutItemStyled gridArea="input">
      <SearchInput />
    </LayoutItemStyled>
    <LayoutItemStyled gridArea="sidebar">
      <LatestItems />
    </LayoutItemStyled>
    <LayoutItemStyled gridArea="weather">
      <WeatherBlock />
    </LayoutItemStyled>
  </LayoutStyled>
);

Layout.propTypes = {};

export default Layout;
