import styled, { css } from 'styled-components';
import { media } from '../../../utils/styled/media';

const LayoutItemStyled = styled.div`
  ${({ gridArea, theme }) => css`
    grid-area: ${gridArea};

    ${gridArea === 'sidebar' &&
      css`
        overflow-y: auto;
        padding-right: 20px;
      `};

    ${gridArea === 'info' &&
      css`
        display: none;
        ${media.md} {
          display: block;
          margin-right: 20px;
          border: 1px solid ${theme.colors.primary};
          box-shadow: ${theme.shadows.default};
        }
      `};

    ${gridArea === 'weather' &&
      css`
        margin-right: 20px;
        border: 1px solid ${theme.colors.primary};
        box-shadow: ${theme.shadows.default};
        padding: 20px;
      `};
  `};
`;

export default LayoutItemStyled;
