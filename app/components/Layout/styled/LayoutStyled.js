import styled from 'styled-components';

import { media } from '../../../utils/styled/media';

const LayoutStyled = styled.main`
  display: grid;
  height: 100vh;
  grid-template-areas:
    'input'
    'sidebar'
    'weather';
  grid-template-columns: 100%;
  grid-template-rows: auto auto auto;
  grid-row-gap: 20px;
  padding: 15px;

  ${media.md} {
    grid-template-areas:
      'info     input'
      'sidebar  weather';
    grid-template-columns: 300px auto;
    grid-template-rows: 60px 1fr;
    grid-column-gap: 14px;
    grid-row-gap: 14px;
    padding: 20px;
  }

  ${media.lg} {
    grid-template-areas:
      'info     input'
      'sidebar  weather';
    grid-template-columns: 440px auto;
    grid-template-rows: 100px 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 20px;
    padding: 30px;
  }
`;

export default LayoutStyled;
