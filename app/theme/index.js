import colors from './colors';
import transition from './transition';
import shadows from './shadows';

export default {
  colors,
  transition,
  shadows,
};
