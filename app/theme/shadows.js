import colors from './colors';

export default {
  default: `16px 14px 0 ${colors.primaryLight}`,
  defaultHover: `10px 8px 0 ${colors.primaryLight}`,
  defaultActive: `6px 4px 0 ${colors.primaryLight}`,
};
