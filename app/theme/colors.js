export default {
  primary: '#d0d0d0',
  primaryLight: '#f4f4f4',
  text: '#6b6b6b',
  border: '#f4f4f4',
};
