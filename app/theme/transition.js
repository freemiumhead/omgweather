export default {
  default: 'transition: all 0.25s ease-out',
  shadowTransform: 'translate(10px, 8px)',
  shadowActiveTransform: 'translate(12px, 10px)',
};
