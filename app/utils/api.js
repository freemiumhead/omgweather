import axios from 'axios';

const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
};

const parseJSON = response => {
  if (response.status === 204 || response.status === 205) return null;
  const { data } = response;
  return data;
};

const parseQuery = query =>
  Object.keys(query)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(query[key])}`)
    .join('&');

const formatPathWithQuery = (url, query) => {
  const queryString = parseQuery(query);

  return query ? `${url}?${queryString}` : url;
};

const METHODS = {
  GET: 'GET',
  POST: 'POST',
  PATCH: 'PATCH',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

export const fetch = axios.create({
  headers: {
    common: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  },
});

const generateRequest = method => (url, data, options = {}) =>
  fetch({
    url: formatPathWithQuery(url),
    ...options,
    method,
    data,
  })
    .then(checkStatus)
    .then(parseJSON);

const generateGetRequest = method => (url, query = '', options = {}) =>
  fetch({
    url: formatPathWithQuery(url, query),
    ...options,
    method,
  })
    .then(checkStatus)
    .then(parseJSON);

export const getRequest = generateGetRequest(METHODS.GET);
export const postRequest = generateRequest(METHODS.POST);
export const deleteRequest = generateRequest(METHODS.DELETE);
export const patchRequest = generateRequest(METHODS.PATCH);
export const putRequest = generateRequest(METHODS.PUT);
