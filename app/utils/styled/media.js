const getMedia = dimension => `@media screen and (min-width: ${dimension}px)`;

export const media = {
  sm: getMedia(768),
  md: getMedia(1024),
  lg: getMedia(1440),
};
