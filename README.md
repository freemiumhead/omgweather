# omgweather

Project bootstrapped on [react-boilerplate](https://github.com/react-boilerplate/react-boilerplate)

### Live demo

[omgweather](http://omgweather.duck.frmhd.xyz/)

### How to use

`npm i` - install all packages

`npm start` - start application in dev mode

`npm run start:production` - start application in prod mode

## TODO
- More weather info in main block
- Clean all overhead dependencies from boilerplate
- Cache google autocomplete requests with service-workers
- API keys must be in axios instance
- Remove `react-places-autocomplete`
- Styling on mobile
- Offline (user must have see recently requests in offline)
